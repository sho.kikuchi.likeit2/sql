create database Q1 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

use Q1;
CREATE TABLE item_category(
category_id int PRIMARY KEY AUTO_INCREMENT,
category_name VARCHAR(256) NOT NULL
);
